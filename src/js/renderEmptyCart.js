const storeCartList = document.querySelector('.store-cart__list');

function renderEmptyCart() {
  const cartEmptyElement = document.createElement('div');
  cartEmptyElement.classList.add('store-cart__empty');
  cartEmptyElement.innerHTML = `
    <div class="store-cart__empty__title">Your cart is empty!
    </div>
    <a href="/product.html"><button class="btn button">GO SHOPPING</button></a>
  `;
  storeCartList.append(cartEmptyElement);
}

export default renderEmptyCart;
