const storeCartOverlay = document.querySelector('.store-cart__overlay');
const storeCartList = document.querySelector('.store-cart__list');
const storeCartTotalPrice = document.querySelector('.store-cart__total-price');

const closeCart = () => {
  storeCartList.innerHTML = '';
  storeCartTotalPrice.innerHTML = '';
  storeCartOverlay.classList.remove('active');
};

export default closeCart;
