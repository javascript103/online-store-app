import createCartProductHTML from './createCartProductHTML';

const storeCartList = document.querySelector('.store-cart__list');

const renderCartProductList = (cart) => {
  const order = [];
  cart.forEach((item) => {
    const index = order.findIndex((el) => el.id === item.id);
    if (index >= 0) {
      // eslint-disable-next-line no-plusplus
      order[index].count++;
    } else {
      // eslint-disable-next-line no-param-reassign
      item.count = 1;
      order.push(item);
    }
  });
  order.forEach((item) => {
    const cartProductCard = createCartProductHTML(item);
    storeCartList.append(cartProductCard);
  });
};

export default renderCartProductList;
