function createCartProductHTML(productData) {
  const cartProductHTMLElement = document.createElement('div');
  cartProductHTMLElement.classList.add('store-cart__list__item');

  cartProductHTMLElement.innerHTML = `
    <div class="store-cart__list__item__wrapper">
      <img src="${productData.imgURL}" alt="Product">
    </div>
    <div class="store-cart__list__item__name">
      <p class="store-cart__list__item__name-title">${productData.name}</p>
      <p class="store-cart__list__item__name-desc">Company: ${productData.company}</p>
    </div>
    <div class="store-cart__list__item__amount">
      <button class="button-amount minus" data-minusId="${productData.id}"></button>
      <span class="store-cart__list__item__amount-number">${productData.count}</span>
      <button class="button-amount plus" data-plusId="${productData.id}"></button>
    </div>
    <div class="store-cart__list__item__price">${productData.price} $
    </div>
    <button class="button-remove" data-removeid="${productData.id}">
    </button>
  `;
  return cartProductHTMLElement;
}

export default createCartProductHTML;
