function renderCartTotalPrice(cart) {
  const totalPrice = cart.reduce((sum, curr) => (sum + curr.price), 0);
  const storeCartTotalPrice = document.querySelector('.store-cart__total-price');
  storeCartTotalPrice.innerHTML = `
    <div class="store-cart__total-price__title">Total: ${totalPrice} $
    </div>
    <a href="#"><button class="btn button">CHECKOUT</button></a>
  `;
  return renderCartTotalPrice;
}

export default renderCartTotalPrice;
