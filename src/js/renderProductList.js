import product from '../data/products.json';
import createProductHTML from './createProductHTML';

const productList = document.querySelector('.store-product');

function renderProductList() {
  product.forEach((item) => {
    const productCard = createProductHTML(item);
    productList.append(productCard);
  });
}

export default renderProductList;
