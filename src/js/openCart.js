import renderCartProductList from './renderCartProductList';
import renderCartTotalPrice from './renderCartTotalPrice';
import renderEmptyCart from './renderEmptyCart';
import storageService from './localStorage';

const storeCartOverlay = document.querySelector('.store-cart__overlay');

const openCart = () => {
  const cart = storageService.get('cart') || [];
  if (cart.length > 0) {
    renderCartProductList(cart);
    renderCartTotalPrice(cart);
  } else {
    renderEmptyCart();
  }
  storeCartOverlay.classList.add('active');
};

export default openCart;
