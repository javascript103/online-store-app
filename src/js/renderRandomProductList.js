import createProductHTML from './createProductHTML';

const productListRandom = document.querySelector('.store-feature__product-random');

const getRandomIndex = (max) => {
  const r = Math.floor(Math.random() * max);
  return r;
};

function renderRandomProductList(product) {
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < 4; i++) {
    const randomProductCard = createProductHTML(product[getRandomIndex(product.length)]);
    productListRandom.append(randomProductCard);
  }
}

export default renderRandomProductList;
