import storageService from './localStorage';
import { resetCartProductList } from './removeProductFromCart';
import renderCartProductList from './renderCartProductList';
import updateCounter from './updateCounter';
import renderCartTotalPrice from './renderCartTotalPrice';

const updateCartList = (cart) => {
  storageService.set('cart', cart);
  resetCartProductList();
  renderCartProductList(cart);
  updateCounter(cart);
  renderCartTotalPrice(cart);
};

const productChangeAmount = (event) => {
  const { target } = event;
  const minusId = target.dataset.minusid;
  const plusId = target.dataset.plusid;
  const cart = storageService.get('cart');
  if (minusId) {
    const index = cart.findIndex((item) => item.id === parseInt(minusId, 10));
    if (index >= 0) {
      cart.splice(index, 1);
      updateCartList(cart);
    }
  }
  if (plusId) {
    const productToAdd = cart.find((item) => item.id === parseInt(plusId, 10));
    cart.push(productToAdd);
    updateCartList(cart);
  }
};

export default productChangeAmount;
