import storageService from './localStorage';
import renderCartProductList from './renderCartProductList';
import updateCounter from './updateCounter';
import renderCartTotalPrice from './renderCartTotalPrice';
import renderEmptyCart from './renderEmptyCart';

export const resetCartProductList = () => {
  const storeCartList = document.querySelector('.store-cart__list');
  storeCartList.innerHTML = '';
};

const resetCartTotalPrice = () => {
  const storeCartTotalPrice = document.querySelector('.store-cart__total-price');
  storeCartTotalPrice.innerHTML = '';
};

const removeProductFromCart = (event) => {
  const { target } = event;
  const productToRemoveId = target.dataset.removeid;
  if (productToRemoveId) {
    const cart = storageService.get('cart');
    const updatedCart = cart.filter((item) => item.id !== parseInt(productToRemoveId, 10));
    storageService.set('cart', updatedCart);
    resetCartProductList();
    renderCartProductList(updatedCart);
    if (updatedCart.length > 0) {
      renderCartTotalPrice(updatedCart);
    } else {
      resetCartTotalPrice();
      renderEmptyCart(updatedCart);
    }
    updateCounter(updatedCart);
  }
};

export default removeProductFromCart;
