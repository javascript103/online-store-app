const updateCounter = (cart) => {
  if (!cart) return;
  const counter = document.querySelector('.counter');
  if (cart.length > 0) {
    counter.textContent = cart.length;
    if (!counter.classList.contains('active')) {
      counter.classList.add('active');
    }
  } else {
    counter.classList.remove('active');
  }
};

export default updateCounter;
