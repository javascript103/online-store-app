import storageService from './localStorage';
import product from '../data/products.json';
import updateCounter from './updateCounter';

const addProductToCart = (event) => {
  const { target } = event;
  const productAddId = target.dataset.id;
  if (productAddId) {
    const productToAdd = product.find((item) => item.id === parseInt(productAddId, 10));
    const cart = storageService.get('cart') || [];
    cart.push(productToAdd);
    storageService.set('cart', cart);
    updateCounter(cart);
  }
};

export default addProductToCart;
