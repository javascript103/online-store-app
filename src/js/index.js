import '../styles/reset.css';
import '../styles/style.scss';

import product from '../data/products.json';
import renderProductList from './renderProductList';
import renderRandomProductList from './renderRandomProductList';
import addProductToCart from './addProductToCart';
import closeCart from './closeCart';
import removeProductFromCart from './removeProductFromCart';
import openCart from './openCart';
import storageService from './localStorage';
import updateCounter from './updateCounter';
import productChangeAmount from './productChangeAmount';

function app() {
  console.log('App start...');
  const buttonCartOpen = document.querySelector('.button-cart-open');
  const buttonCartClose = document.querySelector('.button-cart-close');
  const featureListContainer = document.querySelector('.store-feature__product-random');
  const productListContainer = document.querySelector('.store-product');
  const storeCartListContainer = document.querySelector('.store-cart__list');
  const cart = storageService.get('cart');
  console.log('Initialize components');
  console.log({ buttonCartOpen });
  console.log({ buttonCartClose });
  console.log({ featureListContainer });
  console.log({ productListContainer });
  console.log({ storeCartListContainer });
  console.log({ cart });

  if (featureListContainer) {
    renderRandomProductList(product);
    featureListContainer.addEventListener('click', addProductToCart);
  }
  if (productListContainer) {
    renderProductList(product);
    productListContainer.addEventListener('click', addProductToCart);
  }

  updateCounter(cart);

  buttonCartOpen.addEventListener('click', openCart);
  buttonCartClose.addEventListener('click', closeCart);
  storeCartListContainer.addEventListener('click', removeProductFromCart);
  storeCartListContainer.addEventListener('click', productChangeAmount);
}

document.addEventListener('DOMContentLoaded', app);
